package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type Books struct {
	Books []Book `json:"book"`
}

// Describe an individual book
type Book struct {
	ID       string `json:"id"`
	Language string `json:"language"`
	Edition  string `json:"edition"`
	Author   string `json:"author"`
}

// Load file from disk, convert to string
func readFile(filename string) (contents []byte) {
	contents, err := os.ReadFile(filename)
	if err != nil {
		fmt.Printf("File was not able to be loaded. Error: %s", err)
		os.Exit(1)
	}
	return
}

func writeFile(filename string, data []byte) (contents []byte) {
	err := os.WriteFile(filename, data, 0644)
	if err != nil {
		fmt.Printf("Unable to write data to file. Error: %s", err)
		os.Exit(1)
	}
	return
}

// Unmarshall byte slice to Data struct
func unmarshallData(bdata []byte) (sdata Books) {
	err := json.Unmarshal(bdata, &sdata)
	if err != nil {
		fmt.Printf("Cannot unmarshall data. Error:\n%s", err)
	}
	return
}

// Marshall struct to json string
func marshallData(sdata *Books) (jdata []byte) {
	jdata, err := json.Marshal(sdata)
	if err != nil {
		fmt.Printf("Error marshalling struct to json. Error:%v\n", err)
		os.Exit(1)
	}
	return
}

func main() {
	// define the file to read from.
	inFile := readFile("test_data.json")
	outFile := "changed_data.json"
	myBooks := unmarshallData(inFile)

	fmt.Printf("Book 1 before transform:\t%v\n", myBooks.Books[0])
	myBooks.Books[0].Edition = "fourth"
	fmt.Printf("Book 1 after transform:\t\t%v\n", myBooks.Books[0])

	outData := marshallData(&myBooks)

	writeFile(outFile, outData)

}
